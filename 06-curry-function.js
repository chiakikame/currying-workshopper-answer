module.exports = function(func) {
  const expectedArgc = func.length;
  let args = [];
  const context = this;
  
  return function argCollector() {
    args = args.concat([...arguments]);
    //console.log(args);
    if (args.length >= expectedArgc) {
      return func.apply(context, args);
    } else {
      return argCollector;
    }
  }
};
