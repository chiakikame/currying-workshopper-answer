// Although this implementation passes the test,
// it does not fit the requirement of 'returning self'

module.exports = (a) => {
  let s = a;
  return function adder() {
    if (arguments.length === 0) {
      return s;
    } else {
      s += arguments[0];
      return adder;
    }
  }
};
